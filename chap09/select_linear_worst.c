/* FILE -- select_linear_worst.c
 *
 * Select the ith smallest item of a array in linear time in worst case
 */

#include <stdio.h>
#include <stdlib.h>

inline void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

/* return the index-th smallest item in base[] 
 *
 * Because we don't generate temp array for storing, we introduce a *gap*
 * variable indicating how the array is grouped. For example, set gap=5 to
 * simulate selection of a[0] a[4] a[9] ... 
 *
 * =====> Warning <===== 
 * This really spoil the code. Make the code hard to understand and debug. 
 * =====> Warning End <=====
 *
 * left and right should be compatiable with gap, that is (right % gap == 0).
 * And right should point to the right most item in the array. Of course,
 * these are all garenteed in the code. */
int linear_select(int base[], int left, int right, int index, int gap)
{
    if ((right - left)/gap == 0) {
        return base[left];
    }

    /* 1. group the array into 5s and select their median by doing an
     * insertion sort */
    int i,j,k;

    /* for every group(each contains 5 items) */
    for (i=left; i <= right; i+=5*gap) {
        /* normalize the case: for the last group, maybe less than 5 items */
        int end_point = ((i+4*gap)<right)?(i+4*gap):(right);
        
        /* doing insertion sort */
        for (j=i+gap; j<=end_point; j+=gap) {
            for (k=j; k>i; k-=gap) {
                if (base[k] < base[k-gap]) {
                    swap(&base[k], &base[k-gap]);
                }
            }
        }

        /* if the number of input is less then 5, give the anser directly */
        if ((right - left) < 5*gap) {
            return base[index+left];
        }

        /* put the median to the first of the 5 places. this is used to make
         * compatitable with *gap* strategy */
        int mid = (end_point-i)/gap/2*gap + i;
        swap(&base[i], &base[mid]);
    }


    /* 2. call Select function recursively to get the median of the
     * groups(n/5) */
    int next_gap = 5*gap;
    int mid_value = linear_select(base, 
                                  (left+next_gap-1)/next_gap*next_gap,
                                  right/next_gap*next_gap,
                                  (right-left)/next_gap/2*next_gap,
                                  next_gap);

    /* 3. doing partition */

    /* debug */
    int run_left = left;
    int run_right = right;
    do {
        while (base[run_left] < mid_value) {
            run_left += gap;
        }
        while (mid_value < base[run_right]) {
            run_right -= gap;
        }

        if (run_left < run_right) {
            swap(&base[run_left], &base[run_right]);
            if (base[run_left] == mid_value) {
                run_right -= gap;
            } else if (base[run_right] == mid_value) {
                run_left += gap;
            } else {
                run_left += gap;
                run_right -= gap;
            }
        } else if (run_left == run_right) {
            run_left += gap;
            run_right -= gap;
            break;
        }
    } while (run_left <= run_right);

    /* 4. Recursively doing selection */
    int size_left = run_left - left;
    if (index == size_left - gap) {
        return base[left+index];
    } else if (index < size_left - gap) {
        return linear_select(base, left, (run_left-2*gap)/gap*gap, index, gap);
    } else {
        return linear_select(base, (run_left)/gap*gap, right, index-size_left, gap);
    }
}

/******************************************************************************
 * Test case
 *****************************************************************************/
void print_array(int *base, int nmemb)
{
    int i;
    for (i = 0; i < nmemb; i++) {
        printf("%4d", base[i]);
        if (i%10 == 9) {
            printf("\n");
        } else {
            printf(" ");
        }
    }
}

int cmp(const void *a, const void *b)
{
    return (*(int *)a - *(int *)b);
}

int main(int argc, const char *argv[])
{
    #define ARRAY_SIZE 1638
    srand(time(NULL));

    int i,j,x,result;
    int array[ARRAY_SIZE];
    int backup[ARRAY_SIZE];

    int nCases = 1000;
    for (i = 1; i <=nCases; i++) {
        for (j = 0; j < ARRAY_SIZE; j++) {
            backup[j] = array[j] = rand()%1000 - 500;
        }

        x = rand() % ARRAY_SIZE;
        result = linear_select(array, 0, ARRAY_SIZE-1, x, 1);
        qsort(array, ARRAY_SIZE, sizeof(*array), cmp);

        if (result == array[x]) {
            printf("Case %3d: Passed. index = %d, Get: %d, Expected: %d\n", i, x, result, array[x]);
        } else {
            printf("Case %3d: Failed. index = %d, Get: %d, Expected: %d\n", i, x, result, array[x]);
            printf("\nOriginal:\n");
            print_array(backup, ARRAY_SIZE);
            printf("\nSorted\n");
            print_array(array, ARRAY_SIZE);
            printf("\n\n");
        } 
    }

    return 0;
}
