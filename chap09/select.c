/* FILE -- select.c
 *
 * Implement selection in expected linear time */

#include <stdio.h>
#include <stdlib.h>

int rand_range(int min, int max)
{
    srand(time(NULL));
    
    /* in case we passed argument min>max */
    if (min <= max) {
        return (rand()%(max-min+1)+min);
    } else {
        return 0;
    } 
}

inline void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

/* select a random pivot in base[] and doint partion like qsort 
 * base[] => [.(<=pivot) ... pivot ... (>=pivot) .] 
 */
int randomized_partition(int base[], int left, int right)
{
    srand(time(NULL));

    int pivot = rand_range(left, right); /* pivot as index */
    swap(&base[pivot], &base[right]);

    /* doing partition */
    int i,j;
    i = left - 1;
    pivot = base[right]; /* pivot as value */
    for (j=left; j<right; j++) {
        if (base[j] < pivot) {
            ++i;
            swap(&base[j], &base[i]);
        } 
    }
    swap(&base[i+1], &base[right]);
    return i+1;
}

/* implement a qsort */
void rqsort(int base[], int left, int right)
{
    if (left < right) {
        int mid = randomized_partition(base, left, right);
        rqsort(base, left, mid-1);
        rqsort(base, mid+1, right);
    } 
}

/* implement selection 
 * select the ith element of an array, split it into two parts
 * [ (x elements <mid) mid (y elements >=mid) ]
 * if i<x => the desirable item lies on the left side
 * if i>x => the desirable item lies on the right side. 
 *
 * Note: index starts from 0 */
int randomized_select(int base[], int left, int right, int index)
{
    if (left == right) {
        return base[left];
    } 

    int mid = randomized_partition(base, left, right);
    int size_left = mid - left + 1;

    if (index == size_left-1) {
        return base[mid];
    } else if (index < size_left-1) {
        return randomized_select(base, left, mid-1, index);
    } else {
        return randomized_select(base, mid+1, right, index-size_left);
    } 
}


/******************************************************************************
 * Test case
 *****************************************************************************/
void print_array(int *base, int nmemb)
{
    int i;
    for (i = 0; i < nmemb; i++) {
        printf("%4d", base[i]);
        if (i%10 == 9) {
            printf("\n");
        } else {
            printf(" ");
        }
    }
}

int main(int argc, const char *argv[])
{
    #define ARRAY_SIZE 50
    srand(time(NULL));

    int i,j,x,result;
    int array[ARRAY_SIZE];
    int backup[ARRAY_SIZE];

    int nCases = 20;
    for (i = 1; i <=nCases; i++) {
        for (j = 0; j < ARRAY_SIZE; j++) {
            backup[j] = array[j] = rand()%1000 - 500;
        }

        x = rand() % ARRAY_SIZE;
        result = randomized_select(array, 0, ARRAY_SIZE-1, x);
        rqsort(array, 0, ARRAY_SIZE-1);

        if (result == array[x]) {
            printf("Case %3d: Passed. Get: %d, Expected: %d\n", i, result, array[x]);
        } else {
            printf("Case %3d: Failed. Get: %d, Expected: %d\n", i, result, array[x]);
            print_array(backup, ARRAY_SIZE);
        } 
    }

    return 0;
}
