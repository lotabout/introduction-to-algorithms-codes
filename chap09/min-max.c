/******************************************************************************
 * FILE -- min-max.c
 *
 * find out the minimum and maximum in an array 
 *****************************************************************************/

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

/* C have no way to return two object at once, so we use a structure instead
 */
typedef struct {
    int min;
    int max;
} min_max_t;

/* We will compare the input in pairs and compair the smaller one to min and
 * larger one to max. It cost 3(n/2) in total instead of 2n-2 */
min_max_t get_min_max(int base[], int nmemb)
{
    min_max_t ret;

    int i, smaller, larger;
    ret.min = INT_MAX;
    ret.max = INT_MIN;

    /* if nmemb is odd, we process nmemb-1 items which could be divided into
     * pairs and then the last one item.
     * if nmemb is even, directly divided them into pairs */
    int r_index = (nmemb >> 1) << 1; /* floor(n/2) */
    for (i = 0; i < r_index; i+=2) {
        if (base[i] < base[i+1]) {
            smaller = base[i];
            larger = base[i+1];
        } else {
            smaller = base[i+1];
            larger = base[i];
        } 

        if (ret.min > smaller) {
            ret.min = smaller;
        } 

        if (ret.max < larger) {
            ret.max = larger;
        } 
    }

    /* deal with the last element if nmemb is even */
    if ((nmemb & 0x01) == 1) {
        smaller = base[nmemb-1];
        larger = base[nmemb-1];
    } 
    if (ret.min > smaller) {
        ret.min = smaller;
    } 

    if (ret.max < larger) {
        ret.max = larger;
    } 

    return ret;
}

/******************************************************************************
 * Test case
 *****************************************************************************/
void print_array(int *base, int nmemb)
{
    int i;
    for (i = 0; i < nmemb; i++) {
        printf("%4d", base[i]);
        if (i%10 == 9) {
            printf("\n");
        } else {
            printf(" ");
        }
    }
}

int cmp(const void *a, const void *b)
{
    return (*(int*)a - *(int*)b);
}

int main(int argc, const char *argv[])
{
    #define ARRAY_SIZE 53
    srand(time(NULL));

    int i;
    int array[ARRAY_SIZE];
    for (i = 0; i < ARRAY_SIZE; i++) {
        array[i] = rand()%1000 - 500;
    }

    min_max_t min_max = get_min_max(array, ARRAY_SIZE);
    printf("The array is:\n");
    print_array(array, ARRAY_SIZE);

    qsort(array, ARRAY_SIZE, sizeof(*array), cmp);
    printf("\nArray after sorted:\n");
    print_array(array, ARRAY_SIZE);

    printf("\nmin = %d\tmax = %d\n", min_max.min, min_max.max);
    
    return 0;
}
