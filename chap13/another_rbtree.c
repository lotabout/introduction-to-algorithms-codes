/* filename: another_rbtree.c
 *
 * Implement red-black tree.
 *
 * How to use:
 * suppose that you've compile the program and get `another_rbtree`
 *
 * Run the program and input commands as follows:
 * 1. `a <num>` to insert <num> into the tree.
 * 2. `d <num>` to delete <num> from the tree.
 * 3. no other command support.
 *
 * The program will generate the tree in every step into `dot` language,
 * you can then compile it to picture with the help of `graphviz`
 *
 * Example:
 * 1. put the following text into `commands.txt`
 *    a 1
 *    d 1
 *    a 2
 *    a 3
 *    d 1
 *    a 1
 *    a 3
 *    a 2
 *    a 4
 *    d 3
 * 2. run commands `./another_rbtree < commands.txt > pict.gv`
 * 3. compile the generated picture by:
 *    `dot -Tps pict.gv -o pict.ps`
 * 4. view the picture: `okular pict.ps`
 */

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


enum rb_color{
    RB_BLACK,
    RB_RED,
};

struct rb_node
{
    void *data;

    enum rb_color color;
    struct rb_node *parent;
    struct rb_node *left;
    struct rb_node *right;
};

struct rb_tree
{
    struct rb_node *root;
};

/* function to compare nodes, just like the one used in qsort. */
typedef int (*compr_func)(const void *a, const void *b);

/* generate a new node */
struct rb_node *rb_new_node(void *data)
{
    struct rb_node *new_node = (struct rb_node *)malloc(sizeof(*new_node));
    if (new_node == NULL) {
	fprintf(stderr, "rb_new_node: create_new_node fail, not enough memory.\n");
	exit(-1);
    }

    new_node->data = data;
    new_node->left = NULL;
    new_node->right = NULL;
    new_node->parent = NULL;

    return new_node;
}

void rb_free_node(struct rb_node *node)
{
    if (node != NULL) {
	free(node);
    }
}

/* create a new rb_tree
 * return a pointer to the new tree, NULL if error happens. */
struct rb_tree *rb_new(void)
{
    struct rb_tree *tree = (struct rb_tree *)malloc(sizeof(*tree));

    if (tree != NULL) {
	tree->root = NULL;
    }

    return tree;
}

void rb_traverse(struct rb_node *node, void (*operate)(void *))
{
    if (node == NULL) {
	return;
    }
    if (node->left != NULL) {
	rb_traverse(node->left, operate);
    }
    if (node->right != NULL) {
	rb_traverse(node->right, operate);
    }

    if (operate != NULL) {
	operate(node->data);
    }

    rb_free_node(node);
}

void rb_free(struct rb_tree *tree, void (*free_func)(void *))
{
    rb_traverse(tree->root, free_func);
}

/* Rotate a node to the left
 *   p              p
 *   |              |
 *   n     =>       r
 *    \            /
 *     r          n
 *    /            \
 *   rl             rl
 */
void rotate_left(struct rb_tree *tree, struct rb_node *node)
{
    /* 0. check if the node is avaliable to rotate( r != NULL ) */
    if (node == NULL || node->right == NULL)
	return;

    /* 1. get the parent's child field */
    struct rb_node **child_field = NULL;
    if (node == tree->root)
	child_field = &(tree->root);
    else if (node == node->parent->left)
	child_field = &(node->parent->left);
    else if (node == node->parent->right)
	child_field = &(node->parent->right);

    struct rb_node *r = node->right;

    /* 2. modify parent's child filed to r */
    *child_field = r;
    r->parent = node->parent;

    /* 3. add *rl* to n's right field */
    node->right = r->left;
    if (r->left) {
	r->left->parent = node;
    }

    /* 4. add *n* to *r*'s left child */
    r->left = node;
    node->parent = r;
}

/* Rotate a node to the right
 *   p              p
 *   |              |
 *   n     =>       l
 *  /                \ 
 * l                  n
 *  \                /
 *   lr             lr 
 */
void rotate_right(struct rb_tree *tree, struct rb_node *node)
{
    /* 0. check if the node is avaliable to rotate( l != NULL ) */
    if (node == NULL || node->left == NULL)
	return;

    /* 1. get the parent's child field */
    struct rb_node **child_field = NULL;
    if (node == tree->root)
	child_field = &(tree->root);
    else if (node == node->parent->left)
	child_field = &(node->parent->left);
    else if (node == node->parent->right)
	child_field = &(node->parent->right);

    struct rb_node *l = node->left;

    /* 2. modify parent's child filed to l */
    *child_field = l;
    l->parent = node->parent;

    /* 3. add *lr* to n's left field */
    node->left = l->right;
    if (l->right) {
	l->right->parent = node;
    }

    /* 4. add *n* to *l*'s right child */
    l->right= node;
    node->parent = l;
}

void rb_insert_fixup(struct rb_tree *tree, struct rb_node *node)
{
    while (node->parent && node->parent->color == RB_RED) {
	/* Get the grand parent node, note that this will not be NULL */
	struct rb_node *gparent = node->parent->parent; 
	struct rb_node *parent = node->parent;

	if (parent == gparent->left) {
	    struct rb_node *uncle = gparent->right;

	    if (uncle && uncle->color == RB_RED) {
		/* Case 1 -- color flips
		 *     G             g
		 *    / \           / \
		 *   p   u  =>     P   U
		 *   |             |
		 *   n             n
		 * However, since g's parent might be RED, and property
		 * 4) does not allow this, we have to recurse at g
		 */
		parent->color = RB_BLACK;
		uncle->color = RB_BLACK;
		gparent->color = RB_RED;
		node = gparent;
		continue;
	    }

	    if (node == parent->right) {
		/* Case 2 -- left rotate of parent
		 *     G             G
		 *    / \           / \
		 *   p  (u)    =>  n  (u)  
		 *    \           / 
		 *     n         p
		 * This still violate property 4), the continuation of case 3
		 * will finally fix this.
		 */
		rotate_left(tree, parent);
		struct rb_node *tmp = parent;
		/* update node information for case 3 */
		parent = node;
		node = parent;
	    }

	    /* Case 3 -- right rotate at gparent
	     *     G             P
	     *    / \           / \
	     *   p   U/nil =>  n   g
	     *  /                   \
	     * n                    U/nil
	     */
	    rotate_right(tree, gparent);
	    parent->color = RB_BLACK;
	    gparent->color = RB_RED;
	    break;
	}
	/* parent == gparent->right */
	else {
	    struct rb_node *uncle = gparent->left;

	    if (uncle && uncle->color == RB_RED) {
		/* Case 1 -- color flips */
		parent->color = RB_BLACK;
		uncle->color = RB_BLACK;
		gparent->color = RB_RED;
		node = gparent;
		continue;
	    }

	    if (node == parent->left) {
		/* Case 2 -- right rotate of parent */
		rotate_right(tree, parent);
		struct rb_node *tmp = parent;
		/* update information for case 3 */
		parent = node;
		node = parent;
	    }
	    /* Case 3 -- left rotate at gparent */
	    rotate_left(tree, gparent);
	    parent->color = RB_BLACK;
	    gparent->color = RB_RED;
	    break;
	}
    }

    /* change the root color to BLACK */
    tree->root->color = RB_BLACK;
}

/* insert *data* into the *tree*, using *cmp* to compare keys. */
void rb_insert(struct rb_tree *tree, void *data, compr_func cmp)
{
    /* 1. find the right place to insert */
    struct rb_node **child_field = NULL;
    struct rb_node *parent = NULL;

    child_field = &tree->root;
    while (*child_field != NULL) {
	parent = *child_field;
	if (cmp(data, parent->data) == 0) {
	    /* do not insert existing data */
	    return;
	} else if (cmp(data, parent->data) <= 0) {
	    child_field = &parent->left;
	} else {
	    child_field = &parent->right;
	}
    }

    /* 2. create a new node, insert it, and paint it to RED */
    struct rb_node *node = rb_new_node(data);
    *child_field = node;
    node->parent = parent;
    node->color = RB_RED;

    /* 3. adjust the new node */
    rb_insert_fixup(tree, node);
}

struct rb_node *
rb_search_for_node(struct rb_tree *tree, void *data, compr_func cmp)
{
    struct rb_node *p = tree->root;
    while (p) {
	if (cmp(data, p->data) == 0) {
	    return p;
	} else if (cmp(data, p->data) < 0) {
	    p = p->left;
	} else {
	    p = p->right;
	}
    }

    return NULL;
}

/* find the minimal node in a tree(left most one) */
struct rb_node *rb_minimal(struct rb_node *node)
{
    if (node == NULL) {
	return NULL;
    }

    struct rb_node *run_node = node;
    while (run_node->left != NULL) {
	run_node = run_node->left;
    }

    return run_node;
}

/* swap the contents of two nodes without changing the tree structure. */
void swap_contents(struct rb_node *a, struct rb_node *b)
{
    void *tmp = a->data;
    a->data = b->data;
    b->data = tmp;

    enum rb_color tmp_color = a->color;
    a->color = b->color;
    b->color = tmp_color;
}

/* remove a node which has at most one chile,
 * This function is aimed at fixing tree structure. */
struct rb_node *rb_remove_single(struct rb_tree *tree, struct rb_node *node)
{
    struct rb_node **child_field = NULL;
    if (node == tree->root) {
	child_field = &tree->root;
    } else if (node == node->parent->left) {
	child_field = &node->parent->left;
    } else if (node == node->parent->right) {
	child_field = &node->parent->right;
    }

    if (node->left == NULL && node->right == NULL) {
	*child_field = NULL;
    } else if (node->left == NULL) {
	*child_field = node->right;
	node->right->parent = node->parent;
    } else if (node->right == NULL) {
	*child_field = node->left;
	node->left->parent = node->parent;
    }

    return node;
}

void rb_remove_fixup(struct rb_tree *tree, struct rb_node *node)
{
    while (node != tree->root && node->color == RB_BLACK) {
	if (node == node->parent->left) {
	    struct rb_node *sibling = node->parent->right;
	    if (sibling->color == RB_RED) {
		/* Case 1 -- left rotate at parent
		 *     P                S
		 *    / \              / \
		 *   N   s    =>      p   SL
		 *      / \          / \
		 *     SL SR        N  SR
		 */
		rotate_left(tree, node->parent);
		node->parent->color = RB_RED;
		sibling->color = RB_BLACK;

		/* to case 2/3 */
		sibling = node->parent->right;
	    }

	    /* now the sibling of n is BLACK */

	    if ((sibling->left == NULL || sibling->left->color == RB_BLACK) &&
		(sibling->right == NULL || sibling->right->color == RB_BLACK)) {
		/* Case 2 -- sibling color flip
		 *     (p)            (p)  <---- new n
		 *    /   \          /   \
		 *   N     S    =>  N     s
		 *        / \            / \
		 *       SL SR          SL SR
		 * note that parent's color can be either black or red.
		 * if it is black, the iteration will continue, if it is
		 * red, it will break the iteration and be set to BLACK.
		 */
		sibling->color = RB_RED;
		node = node->parent;
		continue;
	    }

	    /* Now the children of sibling has at least one RED */
	    if (sibling->right == NULL || sibling->right->color == RB_BLACK) {
		/* Case 3 -- right rotate at sibling
		 * (p could be either black or red)
		 *     (p)            (p)
		 *    /   \          /   \
		 *   N     S    =>  N    SL
		 *        / \              \
		 *       sl SR              s
		 *                           \
		 *                            SR
		 * (to case 4)
		 */
		rotate_right(tree, sibling);
		sibling->color = RB_RED;
		sibling = node->parent->right;
		sibling->color = RB_BLACK;
	    }

	    /* Case 4 -- left rotate at parent
	     * (p could be either black or red)
	     *     (p)                 (s)
	     *    /   \               /   \
	     *   N     S    =>       P    SR
	     *        / \           / \    
	     *      (sl) sr        N  (sl)   
	     */
	    rotate_left(tree, node->parent);
	    sibling->color = node->parent->color;
	    sibling->right->color = RB_BLACK;
	    node->parent->color = RB_BLACK;

	    node = tree->root;
	}
	/* node == node->parent->right */
	else {
	    struct rb_node *sibling = node->parent->left;
	    if (sibling->color == RB_RED) {
		/* Case 1 -- right rotate at parent */
		rotate_right(tree, node->parent);
		node->parent->color = RB_RED;
		sibling->color = RB_BLACK;
		sibling = node->parent->left;
	    }

	    if (sibling->left->color == RB_BLACK &&
		sibling->right->color == RB_BLACK) {
		/* Case 2 -- sibling color flip */
		sibling->color = RB_RED;
		node = node->parent;
		continue;
	    }

	    if (sibling->left->color == RB_BLACK) {
		/* Case 3 -- left rotate at sibling */
		rotate_left(tree, sibling);
		sibling->color = RB_RED;
		sibling = node->parent->left;
		sibling->color = RB_BLACK;
	    }

	    /* Case 4 -- right rotate at parent */
	    rotate_right(tree, node->parent);
	    sibling->color = node->parent->color;
	    sibling->left->color = RB_BLACK;
	    node->parent->color = RB_BLACK;

	    node = tree->root;
	}
    }

    /* now *node* should be root or node->color == RED */
    if (node != NULL) {
	node->color = RB_BLACK;
    }
}

/* remove a node from RB-tree */
void *rb_remove_node(struct rb_tree *tree, struct rb_node *node)
{
    struct rb_node *node_to_be_removed = node;
    struct rb_node *node_replace = NULL;
    if (node == NULL) {
	return NULL;
    } else if (node->left == NULL) {
	node_replace = node->right;
    } else if (node->right == NULL) {
	node_replace = node->left;
    } else {
	node_to_be_removed = rb_minimal(node->right);
	node_replace = node_to_be_removed->right;

	swap_contents(node, node_to_be_removed);
    }

    /* note that because we don't have NIL node, so, we first fixup the tree
     * and then actually remove the node */
    if (node_to_be_removed->color == RB_BLACK) {
	if (node_replace != NULL) {
	    node_to_be_removed->color = node_replace->color;
	    rb_remove_fixup(tree, node_to_be_removed);
	    node_replace->color = node_to_be_removed->color;
	} else {
	    rb_remove_fixup(tree, node_to_be_removed);
	}
    }
    rb_remove_single(tree, node_to_be_removed);

    void *data = node_to_be_removed->data;
    rb_free_node(node_to_be_removed);

    return data;
}

void *rb_remove(struct rb_tree *tree, void *data, compr_func cmp)
{
    struct rb_node *node = rb_search_for_node(tree, data, cmp);
    if (node) {
	return rb_remove_node(tree, node);
    }

    return NULL;
}

/*=============================================================================
 * Test
 */

typedef char *(*pr_fun)(void *data);

void dump_node(struct rb_node *node, pr_fun print, FILE *fp)
{
    if (node == NULL) {
	return;
    }

    char *parent = print(node->data);
    if (node->color == RB_BLACK) {
	fprintf(fp, "%s[style=filled,color=black, fontcolor=white];\n", parent);
    } else {
	fprintf(fp, "%s[style=filled, color=red, fontcolor=white];\n", parent);
    }

    if (node->left != NULL) {
	char *left = print(node->left->data);
	fprintf(fp, "%s -- %s;\n", parent, left);
	free(left);
	dump_node(node->left, print, fp);
    }
    if (node->right != NULL) {
	char *right = print(node->right->data);
	fprintf(fp, "%s -- %s;\n", parent, right);
	free(right);
	dump_node(node->right, print, fp);
    }

    free(parent);
}

/* dump RB-tree to `dot` language */
void dump_tree(struct rb_tree *tree, pr_fun print,
	       const char *label,
	       const char *filename)
{
    FILE *fp;
    if (filename == NULL) {
	fp = stdout;
    } else {
	fp = fopen(filename, "w");
    }

    fprintf(fp, "graph G {\n");
    if (label != NULL) {
	fprintf(fp, "label=\"%s\"\n", label);
    }
    dump_node(tree->root, print, fp);
    fprintf(fp, "}\n");
    if (fp != stdout) {
	fclose(fp);
    }
}

int cmp(const void *a, const void *b)
{
    return (*((int *)a) - *((int *)b));
}

char *print(void * data)
{
    char *buf = (char *)malloc(sizeof(char) * BUFSIZ);
    sprintf(buf, "%d", *(int *)data);
    return buf;
}

void cmd_insert(struct rb_tree *tree, int num)
{
    int *data = (int *)malloc(sizeof(*data));
    *data = num;
    rb_insert(tree, data, cmp);

    char buf[BUFSIZ];
    sprintf(buf, "Insert %d.", num);
    dump_tree(tree, print, buf, NULL);
}

void cmd_delete(struct rb_tree *tree, int num)
{
    void *data = rb_remove(tree, &num, cmp);

    char buf[BUFSIZ];
    sprintf(buf, "Remove %d.", num);
    dump_tree(tree, print, buf, NULL);
    if (data != NULL) {
	free(data);
    }
}

int main(int argc, const char *argv[])
{
    struct rb_tree *tree = rb_new();

    char *line;
    size_t len;
    ssize_t read;
    int num;

    while ((read = getline(&line, &len, stdin)) != -1) {

	if (line[0] == 'a') {
	    num = atoi(line+1);
	    cmd_insert(tree, num);
	} else if (line[0] == 'd') {
	    num = atoi(line+1);
	    cmd_delete(tree, num);
	} else {
	    fprintf(stderr, "Command not supported: %s.", line);
	}
    }

    free(line);
    rb_free(tree, free);

    return 0;
}
