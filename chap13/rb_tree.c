/* FILE -- rb_tree.c
 *
 * Implement red-black tree
 */

#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

enum rb_color {
    RB_BLACK,
    RB_RED,
};

struct rb_node {
    void *data;

    enum rb_color color;
    struct rb_node *parent;
    struct rb_node *left;
    struct rb_node *right;
};

struct rb_tree {
    struct rb_node *root;
};

typedef int (*compar_fn)(const void *a, const void *b);

struct rb_tree *rb_new(void)
{
    struct rb_tree *new_tree;
    new_tree = (struct rb_tree *)malloc(sizeof(*new_tree));

    if (new_tree != NULL)
        new_tree->root = NULL;

    return new_tree;
}

struct rb_node *create_rb_node(void *data)
{
    struct rb_node *new_node = (struct rb_node *)malloc(sizeof(*new_node));
    if (new_node == NULL) {
        fprintf(stderr, "create_rb_node failed. not enough memory\n");
        exit(-1);
    }

    new_node->data = data;
    new_node->parent = NULL;
    new_node->left = NULL;
    new_node->right = NULL;

    return new_node;
}

void free_rb_node(struct rb_node *node)
{
    if (node != NULL) {
        free(node);
    }
}

void rotate_left(struct rb_tree *tree, struct rb_node *node)
{
    /* 1. get parent's child field */
    struct rb_node **child_field = NULL;
    if (node == tree->root) {
        child_field = &tree->root;
    } else if (node == node->parent->left) {
        child_field = &(node->parent->left);
    } else if (node == node->parent->right) {
        child_field = &(node->parent->right);
    }

    struct rb_node *node_y = node->right;

    /* 2. modify node's parent's corresponding child */
    *child_field = node_y;

    /* 3. construct new tree structure */
    node_y->parent = node->parent;
    node->parent = node_y;

    node->right = node_y->left;
    if (node_y->left) {
        node_y->left->parent = node;
    }
    node_y->left = node;
}

void rotate_right(struct rb_tree *tree, struct rb_node *node)
{
    /* 1. get parent's child field */
    struct rb_node **child_field = NULL;
    if (node == tree->root) {
        child_field = &tree->root;
    } else if (node == node->parent->left) {
        child_field = &(node->parent->left);
    } else if (node == node->parent->right) {
        child_field = &(node->parent->right);
    }

    struct rb_node *node_y = node->left;

    /* 2. modify node's parent's corresponding child */
    *child_field = node_y;

    /* 3. construct new tree structure */
    node_y->parent = node->parent;
    node->parent = node_y;

    node->left= node_y->right;
    if (node_y->right) {
        node_y->right->parent = node;
    }
    node_y->right= node;
}

void rb_insert_fixup(struct rb_tree *tree, struct rb_node *node_z)
{
    while (node_z->parent && node_z->parent->color == RB_RED) {
        /* node's parent is the left child of its grandparent */
        if (node_z->parent == node_z->parent->parent->left) {
            struct rb_node *node_y = node_z->parent->parent->right;
            if (node_y && node_y->color == RB_RED) {
                /* case 1 */
                node_z->parent->color = RB_BLACK;
                node_y->color = RB_BLACK;
                node_y->parent->color = RB_RED;
                node_z = node_z->parent->parent;
            } else if (node_z == node_z->parent->right) {
                /* case 2 */
                node_z = node_z->parent;
                rotate_left(tree, node_z);
            } else {
                /* case 3 */
                node_z->parent->color = RB_BLACK;
                node_z->parent->parent->color = RB_RED;
                rotate_right(tree, node_z->parent->parent);
            }
        }
        /* node is the right child of its parent */
        else {
            struct rb_node *node_y = node_z->parent->parent->left;
            if (node_y && node_y->color == RB_RED) {
                /* case 4 */
                node_z->parent->color = RB_BLACK;
                node_y->color = RB_BLACK;
                node_y->parent->color = RB_RED;
                node_z = node_z->parent->parent;
            } else if (node_z == node_z->parent->left) {
                /* case 2 */
                node_z = node_z->parent;
                rotate_right(tree, node_z);
            } else {
                node_z->parent->color = RB_BLACK;
                node_z->parent->parent->color = RB_RED;
                rotate_left(tree, node_z->parent->parent);
            }
        }
    }
    tree->root->color = RB_BLACK;
}

void rb_insert(struct rb_tree *tree, void *data, compar_fn cmp)
{
    /* 1. search the right place to inseart */
    struct rb_node **child_field = NULL; /* parent's child field */
    struct rb_node *parent = NULL;

    child_field = &tree->root;

    while (*child_field != NULL) {
        parent = *child_field;
        if (cmp(data, parent->data) <= 0) {
            child_field = &(parent->left);
        } else {
            child_field = &(parent->right);
        }
    }

    /* 2. create a new node and color it with RED */
    struct rb_node *new_node = NULL;
    new_node = create_rb_node(data);
    new_node->parent = parent;
    new_node->color = RB_RED;
    *child_field = new_node;

    /* 3. adjust new node */
    rb_insert_fixup(tree, new_node);
}

struct rb_node *
rb_search_for_node(struct rb_tree *tree, void *data, compar_fn cmp)
{
    struct rb_node *node = tree->root;
    while (node != NULL) {
        int cmp_result = cmp(data, node->data);
        if (cmp_result == 0) {
            return node;
        } else if (cmp_result < 0) {
            node = node->left;
        } else {
            node = node->right;
        }
    }

    return NULL;
}

void *rb_search(struct rb_tree *tree, void *data, compar_fn cmp)
{
    struct rb_node *node = rb_search_for_node(tree, data, cmp);
    if (node != NULL) {
        return node->data;
    }

    return NULL;
}

/* deletion */
struct rb_node *rb_minimum(struct rb_node *root)
{
    struct rb_node *run_node = root;
    while (run_node->left != NULL) {
        run_node = run_node->left;
    }

    return run_node;
}

/* remove node that have at most one non-leaf child */
struct rb_node *rb_remove_node(struct rb_tree *tree, struct rb_node *node)
{
    struct rb_node **child_field = &tree->root;
    if (node == tree->root) {
        child_field = &tree->root;
    } else if (node == node->parent->left) {
        child_field = &(node->parent->left);
    } else {
        child_field = &(node->parent->right);
    }

    if (node->left == NULL && node->right == NULL) {
        *child_field = NULL;
    } else if (node->left == NULL) {
        *child_field = node->right;
        node->right->parent = node->parent;
    } else {
        *child_field = node->left;
        node->left->parent = node->parent;
    }

    return node;
}

void rb_delete_fixup(struct rb_tree *tree, struct rb_node *node)
{
    while (node != tree->root && node->color == RB_BLACK) {
        if (node == node->parent->left) {
            struct rb_node *sibling = node->parent->right;

            if (sibling && sibling->color == RB_RED) {
                /* case 1 */
                sibling->color = RB_BLACK;
                node->parent->color = RB_RED;
                rotate_left(tree, node->parent);
                sibling = node->parent->right;
            }

            if ((sibling->left == NULL || sibling->left->color == RB_BLACK) &&
                (sibling->right == NULL || sibling->right->color == RB_BLACK)) {
                /* case 2 */
                sibling->color = RB_RED;
                node = node->parent;
                continue;
            } else if (sibling->right == NULL
                       || sibling->right->color == RB_BLACK) {
                /* case 3 */
                sibling->left->color = RB_BLACK;
                sibling->color = RB_RED;
                rotate_right(tree, sibling);
                sibling = node->parent->right;
            }

            /* case 4 */
            sibling->color = node->parent->color;
            sibling->right->color = RB_BLACK;
            node->parent->color = RB_BLACK;
            rotate_left(tree, node->parent);
            node = tree->root;
        }
        else {
            struct rb_node *sibling = node->parent->left;

            if (sibling && sibling->color == RB_RED) {
                /* case 1 */
                sibling->color = RB_BLACK;
                node->parent->color = RB_RED;
                rotate_right(tree, node->parent);
                sibling = node->parent->left;
            }

            if ((sibling->left == NULL || sibling->left->color == RB_BLACK) &&
                (sibling->right == NULL || sibling->right->color == RB_BLACK)) {
                /* case 2 */
                sibling->color = RB_RED;
                node = node->parent;
                continue;
            } else if (sibling->left == NULL
                       || sibling->left->color == RB_BLACK) {
                /* case 3 */
                sibling->right->color = RB_BLACK;
                sibling->color = RB_RED;
                rotate_left(tree, sibling);
                sibling = node->parent->left;
            }

            /* case 4 */
            sibling->color = node->parent->color;
            sibling->left->color = RB_BLACK;
            node->parent->color = RB_BLACK;
            rotate_right(tree, node->parent);
            node = tree->root;
        }
    }

    node->color = RB_BLACK;
}

void *rb_delete(struct rb_tree *tree, struct rb_node *node)
{
     struct rb_node *node_removal = node;
     enum rb_color color_removal = node_removal->color;
     struct rb_node *subnode_removal = NULL;

     if (node_removal->left == NULL) {
         subnode_removal = node_removal->right;
     } else if (node_removal->right == NULL) {
         subnode_removal = node_removal->left;
     } else {
         node_removal = rb_minimum(tree->root);
         color_removal = node_removal->color;
         subnode_removal = node_removal->right;

         /* swap content */
         void *data = node->data;
         node->data = node_removal->data;
         node_removal->data = data;
     }


     if (node_removal->color == RB_BLACK) {
         if (subnode_removal != NULL) {
             node_removal->color = subnode_removal->color;
             subnode_removal->color = RB_BLACK;
         }
         rb_delete_fixup(tree, node_removal);
     }
     rb_remove_node(tree, node_removal);

     void *data = node_removal->data;
     free_rb_node(node_removal);

     return data;
}

void *rb_remove(struct rb_tree *tree, void *data, compar_fn cmp)
{
    struct rb_node *node = rb_search_for_node(tree, data, cmp);
    return rb_delete(tree, node);
}


/*=============================================================================
 * Test
 */

typedef void (*pr_fun)(void *data);

void print_tree(struct rb_node *node, int level, pr_fun print)
{
    if (node == NULL) {
        return;
    }
    if (node->parent) {
        assert((node->parent->left == node) || (node->parent->right == node));
    }

    print_tree(node->right, level+1, print);
    printf("%*c", level*8, ' ');
    printf("(");
    print(node->data);
    printf(".%c)\n", node->color == RB_BLACK ? 'B':'R');
    print_tree(node->left, level+1, print);
}

int cmp(const void *a, const void *b)
{
    return (*((int *)a) - *((int *)b));
}

void print(void * data)
{
    printf("%d", *(int *)data);
}

int main(int argc, const char *argv[])
{
    int data[20];
    int len = sizeof(data)/sizeof(data[0]);
    int i;

    for (i = 0; i < len; i++) {
        data[i] = i;
    }

    struct rb_tree *tree = rb_new();
    struct rb_node *tmp = NULL;

    /* insertion */
    for (i = 0; i < len; i++) {
        rb_insert(tree, &data[i], cmp);

        printf("===================== %d =====================\n", i);
        print_tree(tree->root, 0, print);
    }


    /* removal */
    printf("============================ After removal ========================\n");
    for (i = 0; i < len/2; i++) {
        rb_remove(tree, &data[i], cmp);
        printf("===================== %d =====================\n", i);
        print_tree(tree->root, 0, print);
    }

    return 0;
}
