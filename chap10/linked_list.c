/* FILE -- linked_list.c
 *
 * Implement doubly linked list for general use
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>

/* we use a satellite node represent list head */
typedef struct _node_t {
    void *data;
    struct _node_t *prev;
    struct _node_t *next;
} node_t;

typedef struct _list_t {
    node_t *head;
    size_t length;
    bool (*match)(const void *, const void *); /* function to determine
                                                  whether two items are the
                                                  same. */
} list_t;

/* allocate a new list node */
node_t *node_alloc(void *data)
{
    node_t *tmp = NULL;
    tmp = (node_t *)malloc(sizeof(*tmp));
    if (tmp == NULL) {
        fprintf(stderr, "node_alloc: Out of memeory, Quit.\n");
        exit(-1);
    } 

    tmp->data = data;
    tmp->prev = NULL;
    tmp->next = NULL;

    return tmp;
}

/* free a node, return the data it contained */
void *node_free(node_t *node)
{
    if (node == NULL) {
        return NULL;
    } 

    void *tmp = node->data;
    free(node);
    return tmp;
}

/* initilize a head list */
void list_init(list_t *lst, bool (*match)(const void *, const void *))
{
    /* allocate a head node for list */
    lst->head = node_alloc(NULL);
    lst->head->prev = lst->head;
    lst->head->next = lst->head;

    lst->length = 0;
    lst->match = match;
}

/* search a key in the list 
 * If nothing found, return NULL. */
node_t *list_search(list_t *lst, void *key)
{
    node_t *run;
    node_t *head = lst->head;
    for (run=head->next; run!=head; run=run->next) {
        if (lst->match(key, run)) {
            return run;
        } 
    }

    return NULL;
}

/* insert a new item after a given node 
 * 
 * Note: before and new should not be NULL.*/
void node_insert(node_t *before, node_t *new)
{
    if (before == NULL || new == NULL) {
        return;
    } 

    new->next = before->next;
    before->next->prev = new;
    new->prev = before;
    before->next = new;
}

/* insert a new data into the linked list */
void list_insert(list_t *lst, void *data)
{
    if (lst == NULL) {
        return ;
    } 
    node_t *new = node_alloc(data);
    node_insert(lst->head, new);
    lst->length ++;
}

/* insert a new data into the tail of the linkded list */
void list_insert_tail(list_t *lst, void *data)
{
    if (lst == NULL) { 
        return;
    } 
    node_t *new = node_alloc(data);
    node_insert(lst->head->prev, new);
    lst->length ++;
}

/* delete a node from the list and return the data stored in the node. */
void *list_delete(list_t *lst, node_t *node)
{
    if (lst == NULL || node == NULL || node == lst->head) {
        return NULL;
    } 

    node->prev->next = node->next;
    node->next->prev = node->prev;
    lst->length --;

    return node_free(node);
}

/* free all items in a list 
 * i.e. destroy a list
 * After calling list_free, lst should not be used again. */
void list_free(list_t *lst, void (*destroy)(void *))
{
    /* destroy items */
    node_t *run;
    node_t *head = lst->head;
    for (run=head->next; run!=head; run=run->next) {
        void *data = list_delete(lst, run);
        destroy(data);
    }

    if (lst->head) {
        free(lst->head);
    } 
    lst->head = NULL;
}

/***************************************************************
 * Doing test
 **************************************************************/
void print_list(list_t *lst)
{
    node_t *run;
    node_t *head = lst->head;
    printf("\nItems in list: [head -- tail]\n[");
    for (run=head->next; run!=head; run=run->next) {
        printf("%d ", *((int*)run->data));
    }
    printf("]\n");
}

bool match(const void *a, const void *b)
{
    if (*(int*)a == *(int *)b) {
        return true;
    } else {
        return false;
    } 
}

/* Use doubly linked list to implement stack */
bool list_empty(list_t *lst)
{
    return (lst->length == 0);
}

void list_push(list_t *lst, void *data)
{
    list_insert(lst, data);
}

void *list_pop(list_t *lst)
{
    if (list_empty(lst)) {
        return NULL;
    } else {
        return list_delete(lst, lst->head->next);
    } 
}

void list_enqueue(list_t *lst, void *data)
{
    list_insert_tail(lst, data);
}

void *list_fetch(list_t *lst)
{
    if (list_empty(lst)) {
        return NULL;
    } else {
        return lst->head->next->data;
    } 
}


int main(int argc, const char *argv[])
{
    int c;
    /* initilize a stack */
    list_t lst;
    list_init(&lst, match);

    int contd = 1;
    while(contd)
    {
        printf("Push(a) | Pop(d) | Enqueue(e) | Get(g) | Print(p) | Quit(q):\n");

        do {
            c = getchar();
        } while ( c=='\n' );

        if (c == EOF) {
            break;
        }

        int *new = NULL;
        int *item = NULL;
        switch(c) {
            case 'a':
                new = malloc(sizeof(*new));
                scanf("%d", new);
                list_push(&lst, (void*)new);

                printf("Newly added on the top: %d\n", *new);
                print_list(&lst);
                new = NULL;
                break;
            case 'e':
                new = malloc(sizeof(*new));
                scanf("%d", new);
                list_enqueue(&lst, (void*)new);

                printf("Newly added to the tail: %d\n", *new);
                print_list(&lst);
                new = NULL;
                break;
            case 'd':
                item = (int*)list_pop(&lst);
                if (item == NULL) {
                    printf("Poped: No item\n");
                } else {
                    printf("Poped: %d\n", *item);
                    print_list(&lst);
                }

                free(item);
                break;
            case 'g':

                item = (int*)list_fetch(&lst);
                if (item == NULL) {
                    printf("Fetched: No item\n");
                } else {
                    printf("Fetched: %d\n", *item);
                    print_list(&lst);
                }

                break;
            case 'p':
                print_list(&lst);
                break;

            case 'q':
                contd = 0;
                break;

            default:
                printf("Wrong command!%c\n", c);
                break;
        }

    }

    list_free(&lst, free);
    
    return 0;
}
