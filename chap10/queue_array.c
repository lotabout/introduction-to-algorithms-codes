/* FILE -- queue_array.c
 *
 * Implement queue using array 
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>

/* tail points to the last unused slot, head points to the first used slot. 
 * [ blank blank data data data data blank blank ... ]
 *                ^                    ^
 *               head                tail 
 */
typedef struct {
    void ** queue;  /* array storing data */
    size_t tail;    /* queue tail index */
    size_t head;    /* queue head index */
    size_t size;    /* the size of *queue* array */
} queue_t;

/* initialize a queue.
 * allocate memeory for storing */
void queue_init(queue_t *queue, size_t size)
{
    /* allocate (size+1) blank spaces. the extra 1 blank space act as a satelite */
    queue->queue = malloc((size+1) * sizeof(*(queue->queue)));
    if (queue->queue == NULL) {
        fprintf(stderr, "queue_init: Out of memeory, quit.\n");
        exit(-1);
    } 

    queue->head = queue->tail = 0;
    queue->size = size+1;
}

/* check if the queue is empty */
bool is_queue_empty(queue_t *queue)
{
    return (queue->head == queue->tail);
}

/* check if the queue is full */
bool is_queue_full(queue_t *queue)
{
    return ((queue->tail+1)%queue->size == queue->head);
}

/* enqueue an item 
 * User have to make sure that the queue is not full before enqueue.
 * if the queue is full, we'll simple ignore the new value. */
void enqueue(queue_t *queue, void *data)
{
    if (is_queue_full(queue)) {
        return;
    } 

    queue->queue[queue->tail] = data;
    queue->tail = (queue->tail + 1) % queue->size;
}

/* dequeue an item
 * User have to make sure that the queue is not empty befor dequeue.
 * If the queue is empty, we'll return NULL. */
void *dequeue(queue_t *queue)
{
    if (is_queue_empty(queue)) {
        return NULL;
    } 
    void *tmp = queue->queue[queue->head];
    queue->head = (queue->head + 1) % queue->size;

    return tmp;
}

/* fetch the first item 
 * Note: User have to make sure that the queue is not empty. Because it will
 * return NULL if the queue is empty, and you might get confused with a empty
 * condition and a NULL data. */
void *queue_fetch(queue_t *queue)
{
    if (is_queue_empty(queue)) {
        return NULL;
    } 

    return queue->queue[queue->head];
}

/* free the elements of a queue 
 * Note: dequeue all items before freeing a queue, otherwise you may lost all
 * data stored in the queue. Also if the elements stored in the queue are
 * pointers to allocated memory, and you forgot to free them before destroy
 * the queue, it may cause memeory leak. */
void queue_free(queue_t *queue)
{
    if (queue == NULL) {
        return ;
    } 

    queue->head = 0;
    queue->tail = 0;
    if (queue->queue != NULL) {
        free(queue->queue);
    } 
    queue->size = 0;
}

/***************************************************************
 * Doing test
 **************************************************************/
void free_queue_items(queue_t *queue)
{
    while (!is_queue_empty(queue)) {
        void * tmp = dequeue(queue);
        if (tmp) {
            free(tmp);
        } 
    }
}

void print_queue(queue_t *queue)
{
    size_t run_head = queue->head;
    printf("\nItems in queue:[head -- tail]\n[");
    for (; run_head != queue->tail; run_head = (run_head+1)%queue->size ) {
        printf("%d ", *((int*)queue->queue[run_head]));
    }
    printf("]\n");
}

int main(int argc, const char *argv[])
{
    int c;
    /* initilize a stack */
    queue_t q;
    queue_init(&q, 10);

    int contd = 1;
    while(contd)
    {
        printf("Enqueue(e) | Dequeue(d) | Get(g) | Print(p) | Quit(q): ");

        do {
            c = getchar();
        } while ( c=='\n' );

        if (c == EOF) {
            break;
        }

        int *new = NULL;
        int *item = NULL;
        switch(c) {
            case 'e':
                new = malloc(sizeof(*new));
                scanf("%d", new);
                if (!is_queue_full(&q)) {
                    enqueue(&q, (void*)new);

                    printf("Newly added: %d\n", *new);
                    print_queue(&q);
                } else {
                    printf("The queue is Full.\n");
                    print_queue(&q);
                    free(new);
                } 

                new = NULL;
                break;
            case 'd':
                item = (int*)dequeue(&q);
                if (item == NULL) {
                    printf("Dequeued: No item\n");
                } else {
                    printf("Dequeued: %d\n", *item);
                    print_queue(&q);
                }

                free(item);
                break;
            case 'g':

                item = (int*)queue_fetch(&q);
                if (item == NULL) {
                    printf("Fetched: No item\n");
                } else {
                    printf("Fetched: %d\n", *item);
                    print_queue(&q);
                }

                break;
            case 'p':
                print_queue(&q);
                break;

            case 'q':
                contd = 0;
                break;

            default:
                printf("Wrong command!%c\n", c);
                break;
        }

    }

    free_queue_items(&q);
    queue_free(&q);
    
    return 0;
}
