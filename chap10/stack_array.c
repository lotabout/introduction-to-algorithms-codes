/* FILE -- stack_array.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>

typedef struct {
    void **stack;
    void **top;
    size_t capacity;
} stack_t;

/* allocate storage for stack */
void stack_init(stack_t *stack, size_t capacity)
{
    stack->capacity = capacity;

    stack->stack = malloc(capacity * sizeof(*(stack->stack)));
    if (stack->stack == NULL) {
        fprintf(stderr, "stack_init: Out of memeory, quit.\n");
        exit(-1);
    } 

    stack->top = stack->stack;
}

/* check whether a stack if full */
inline bool is_stack_full(stack_t *stack)
{
    return ((stack->top - stack->stack) >= stack->capacity);
}

/* check whether a stack if empty */
inline bool is_stack_empty(stack_t *stack)
{
    return (stack->top <= stack->stack);
}

/* expand a stack 
 * This can be called when a stack is full, it will allocate more spaces for
 * the stack. */
void stack_expand(stack_t *stack)
{
    size_t new_capacity = stack->capacity * 2;
    void ** tmp = malloc(new_capacity * sizeof(*(stack->stack)));
    if (tmp == NULL) {
        fprintf(stderr, "stack_expand: Out of memory, quit.\n");
        exit(-1);
    } 
    
    /* copy the data in the old stack to the newly allocated space */
    memcpy(tmp, stack->stack, stack->capacity*sizeof(*(stack->stack)));

    free(stack->stack);
    stack->top = (stack->top - stack->stack) + tmp;
    stack->stack = tmp;
    stack->capacity = new_capacity;
}

/* push a new item into the stack */
void stack_push(stack_t *stack, void *data)
{
    if (is_stack_full(stack)) {
        stack_expand(stack);
    } 
    *(stack->top) = data;
    stack->top ++;
}

/* pop an item out of the stack 
 * Note that it will return NULL if the stack is empty, but if the data is
 * NULL, then we cannot distinguish these two situations. Remember to check if
 * the stack is empty before pop. */
void *stack_pop(stack_t *stack)
{
    if (is_stack_empty(stack)) {
        return NULL;
    } else {
        --(stack->top);
        return (*(stack->top));
    } 
}

/* fetch the top item of the stack 
 * Note: if the stack is empty, fetch will return NULL, so it is unable to
 * distinguish a NULL data from stack empty, thus check whether the stack is
 * empty before fetch */
void *stack_fetch(stack_t *stack)
{
    if (is_stack_empty(stack)) {
        return NULL;
    } else {
        return (*(stack->top-1));
    } 
}

/* Remember to pop out all items before free the stack, because if the stack
 * is the only way that you keep these data, they will lost.
 * Also, if the pointers kept in the stack is not correctly freed, it will
 * cause memory leak */
void stack_free(stack_t *stack)
{
    stack->capacity = 0;
    stack->top = NULL;
    if (stack->stack) {
        free(stack->stack);
    } 
}

/***************************************************************
 * Doing test
 **************************************************************/
void free_stack_items(stack_t *stack)
{
    while (!is_stack_empty(stack)) {
        void *tmp = stack_pop(stack);
        if (tmp) {
            free(tmp);
        } 
    }
}

void print_stack(stack_t *stack)
{
    void ** run_top = stack->top;
    printf("\nItems in stack:[top -- bottom]\n[");
    while ((run_top--) > stack->stack) {
        printf("%d ", *((int*)*run_top));
    }
    printf("]\n");
}

int main(int argc, const char *argv[])
{
    int c;
    /* initilize a stack */
    stack_t stck;
    stack_init(&stck, 10);

    int contd = 1;
    while(contd)
    {
        printf("Push(a) | Pop(d) | Get(g) | Print(p) | Quit(q): ");

        do {
            c = getchar();
        } while ( c=='\n' );

        if (c == EOF) {
            break;
        }

        int *new = NULL;
        int *item = NULL;
        switch(c) {
            case 'a':
                new = malloc(sizeof(*new));
                scanf("%d", new);
                stack_push(&stck, (void*)new);

                printf("Newly added: %d\n", *new);
                print_stack(&stck);
                new = NULL;
                break;
            case 'd':
                item = (int*)stack_pop(&stck);
                if (item == NULL) {
                    printf("Poped: No item\n");
                } else {
                    printf("Poped: %d\n", *item);
                    print_stack(&stck);
                }

                free(item);
                break;
            case 'g':

                item = (int*)stack_fetch(&stck);
                if (item == NULL) {
                    printf("Fetched: No item\n");
                } else {
                    printf("Fetched: %d\n", *item);
                    print_stack(&stck);
                }

                break;
            case 'p':
                print_stack(&stck);
                break;

            case 'q':
                contd = 0;
                break;

            default:
                printf("Wrong command!%c\n", c);
                break;
        }

    }

    free_stack_items(&stck);
    stack_free(&stck);
    
    return 0;
}
