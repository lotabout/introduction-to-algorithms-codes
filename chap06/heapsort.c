/* FILE -- heapsort.c
 * implement heapsort in C and do some test */

#include <stdio.h>

/* function protype for compare two item */
typedef int (*compare)(const void *a, const void *b);

/* we represent heap using array. It can also be regarded as a complete binary
 * tree, the three functions below is used to get the corresponding index in
 * the array given a node index */

/* return the index of the parent node of the current node */
inline int parent(int index)
{
    /* parent(i) = floor((i-1)/2) */
    return ((index-1) >> 1);
}

/* return the index of the left child of the current node */
inline int left(int index)
{
    /* left(i) = 2*i + 1*/
    return ((index << 1)+1);
}

/* return the index of the right child of the current node */
inline int right(int index)
{
    /* right(i) = 2*i + 2 */
    return ((index << 1) + 2);
}

/* heper function: swap */
void swap(void *a, void *b, size_t size)
{
    int i;
    char *pa = (char*)a;
    char *pb = (char*)b;

    if (a == b) {
        return;
    }

    for (i = 0; i < size; ++i,pa++,pb++){
        *pa ^= *pb;
        *pb ^= *pa;
        *pa ^= *pb;
    }
}

/* matain the min-heap property 
 * Params
 *  heap         -- array of data
 *  index        -- current unbalanced index
 *  max_index    -- max index which should not exceeded(heap size)
 *  size         -- size of an item
 *  cmp          -- function to compare items.*/
void min_heapify(void *heap, int index, size_t max_index, size_t size, compare cmp)
{
    int cur = index;
    int l,r;
    int smallest = cur;
    void *p = NULL; /* pointer to the current node */
    void *pl = NULL; /* pointer to the left child */
    void *pr = NULL; /* pointer to the right child */
    void *ps = NULL; /* pointer to the smallest node -- temp pointer */
    while (smallest != max_index) {
        l = left(cur);
        r = right(cur);
        p = heap + cur*size;
        pl = heap + l*size;
        pr = heap + r*size;
        /* check left child to see who is smaller */
        if (l <= max_index && cmp(pl, p) < 0) {
            smallest = l;
            ps = pl;
        } else {
            smallest = cur;
            ps = p;
        }

        /* check the smaller one of the previous comparision and the right
         * child to see who is the smallest one */
        if (r <= max_index && cmp(pr, ps) < 0) {
            smallest = r;
            ps = pr;
        }

        /* exchange heap[cur] with heap[smallest] */
        if (p != ps) {
            swap(p, ps, size);
            cur = smallest;
        } else {
            /* current is the smallest, quit*/
            break;
        }
    }
}


/* matain the max-heap property 
 * Params
 *  heap         -- array of data
 *  index        -- current unbalanced index
 *  max_index    -- max index which should not exceeded(heap size)
 *  size         -- size of an item
 *  cmp          -- function to compare items.*/
void max_heapify(void *heap, int index, size_t max_index, size_t size, compare cmp)
{
    int cur = index;
    int l,r;
    int largest= cur;
    void *p = NULL; /* pointer to the current node */
    void *pl = NULL; /* pointer to the left child */
    void *pr = NULL; /* pointer to the right child */
    void *plargest = NULL; /* pointer to the largest node -- temp pointer */
    while (largest != max_index) {
        l = left(cur);
        r = right(cur);
        p = heap + cur*size;
        pl = heap + l*size;
        pr = heap + r*size;
        /* check left child to see who is larger */
        if (l <= max_index && cmp(pl, p) > 0) {
            largest = l;
            plargest = pl;
        } else {
            largest = cur;
            plargest = p;
        }

        /* check the larger one of the previous comparision and the right
         * child to see who is the largest one */
        if (r <= max_index && cmp(pr, plargest) > 0) {
            largest= r;
            plargest = pr;
        }

        /* exchange heap[cur] with heap[smallest] */
        if (p != plargest) {
            swap(p, plargest, size);
            cur = largest;
        } else {
            /* current is the smallest, quit*/
            break;
        }
    }
}

void build_min_heap(void *heap, size_t nmemb, size_t size, compare cmp)
{
    /* note: nmemb starts from *1* */
    int cur = 0;

    for (cur=(nmemb-1)/2; cur>=0; cur--) {
        min_heapify(heap, cur, nmemb-1, size, cmp);
    }
}

void build_max_heap(void *heap, size_t nmemb, size_t size, compare cmp)
{
    /* note: nmemb starts from *1* */
    int cur = 0;

    for (cur=(nmemb-1)/2; cur>=0; cur--) {
        max_heapify(heap, cur, nmemb-1, size, cmp);
    }
}

/* heap sort -- in ascending/descending order
 * Parameters:
 *  base -- base address of an array
 *  nmemb -- number of items include
 *  size -- the size of item in bytes.
 *  cmp -- a function to determine a>b or a<b.
 * */
void heapsort(void *base, size_t nmemb, size_t size, compare cmp)
{
    /*build_min_heap(base, nmemb, size, cmp);*/
    build_max_heap(base, nmemb, size, cmp);

    int i;
    for (i=nmemb-1; i>0; --i) {
        swap(base, base+i*size, size);
        /*min_heapify(base, 0, i-1, size, cmp);*/
        max_heapify(base, 0, i-1, size, cmp);
    }
}

/* compare function for number comparison */
int cmp(const void *a, const void *b)
{
    return (*(int *)b - *(int*)a);
}

struct st_test {
    int a;
    int b;
};

/* comparison function for struct 
 * {1,1} < {1,2} < {2,1} < {2,2}
 * */
int structcmp(const void *a, const void *b)
{
    struct st_test *x = (struct st_test *)a;
    struct st_test *y = (struct st_test *)b;

    if ((x->a > y->a) || (x->a == y->a && x->b > y->b)) {
        return 1;
    } else if ((x->a < y->a) || (x->a == y->a && x->b < y->b)) {
        return -1;
    } else {
        return 0;
    }
}

int main(int argc, const char *argv[])
{
    int array[] = {2,3,5,1,4,8,3,2,6,2};
    int len = sizeof(array)/sizeof(array[0]);
    heapsort(array, len, sizeof(array[0]), cmp);
    int i;
    for (i = 0; i < len; i++) {
        printf("%d ", array[i]);
    }

    printf("\n");
    struct st_test some[] = {{1,2},{3,5},{8,7},{1,-1},{3,7},{7,78}};
    len = sizeof(some)/sizeof(*some);
    heapsort(some, len, sizeof(*some), structcmp);
    for (i = 0; i < len; i++) {
        printf("{%d, %d} ", some[i].a, some[i].b);
    }

    printf("\n");
    return 0;
}
