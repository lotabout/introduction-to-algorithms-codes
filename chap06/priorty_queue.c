/* FILE -- priority_queue.c
 *
 * Imlement prority queue using heap */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
    size_t nmemb; /* acutual number of member contained in the queue */
    size_t capacity; /* total amount of items can be contained */
    void ** data;    /* actual array of data */
    int (*cmp)(const void *a, const void *b); /* comparator function */
} pQueue_t;

/* Function Declaration */

pQueue_t *pqueue_init(int (*cmp)(const void *a, const void *b), size_t capacity);
void pqueue_free(pQueue_t *pqueue);
void pqueue_enlarge(pQueue_t *pq);
void pqueue_push(pQueue_t *pq, void *item);
void *pqueue_pop(pQueue_t *pq);
void *pqueue_fetch(pQueue_t *pq);
inline int parent(int index);
inline int left(int index);
inline int right(int index);
void pqueue_heapify_bottom(pQueue_t *pq);
void pqueue_heapify_top(pQueue_t *pq);
void pqueue_free_items(pQueue_t *pq);

/* create a new priority queue */
pQueue_t *pqueue_init(int (*cmp)(const void *a, const void *b), size_t
                      capacity)
{
    pQueue_t *pqueue = (pQueue_t *)malloc(sizeof(*pqueue));
    if (pqueue == NULL) {
        fprintf(stderr, "Error: pqueue_init: Out of memeory\n");
        exit(-1);
    }

    pqueue->data = malloc(capacity * sizeof(*(pqueue->data)));
    if (pqueue->data == NULL) {
        fprintf(stderr, "Error: pqueue_init: Out of memeory\n");
        exit(-1);
    }

    pqueue->nmemb = 0;
    pqueue->capacity = capacity;
    pqueue->cmp = cmp;

    return pqueue;
}

/* free all the resoures a queue has */
void pqueue_free(pQueue_t *pqueue)
{
    if (pqueue != NULL) {
        if (pqueue->data != NULL) {
            free(pqueue->data);
        }
        free(pqueue);
    }
}

/* enlarge the capacity of a priority queue */
void pqueue_enlarge(pQueue_t *pq)
{
    int new_capacity = pq->capacity << 1;

    void ** data_new = malloc(new_capacity * sizeof(*data_new));
    if (data_new == NULL) {
        fprintf(stderr, "Error: pqueue_enlarge: Out of memeory\n");
        exit(-1);
    }

    /* copy the original data to the newly allocated place */
    memcpy(data_new, pq->data, pq->capacity * sizeof(*pq->data));
    pq->capacity = new_capacity;
    free(pq->data);
    pq->data = data_new;
}

/* Add a new item to priority queue */
void pqueue_push(pQueue_t *pq, void *item)
{
    /* check if the queue if full 
     * if (queue full) then { enlarge it }; */
    if (pq->nmemb == pq->capacity) {
        pqueue_enlarge(pq);
    }

    /* add the new item to the last and heapify */
    pq->data[pq->nmemb++] = item; 
    pqueue_heapify_bottom(pq);
}

/* Delete the top item from priority queue */
void *pqueue_pop(pQueue_t *pq)
{
    if (pq->nmemb == 0) {
        return NULL;
    }

    /* move the last item to the top and hepify */
    void *item = pq->data[0];
    pq->data[0] = pq->data[--pq->nmemb];
    pqueue_heapify_top(pq);

    return item;
}

/* Return the top item */
void *pqueue_fetch(pQueue_t *pq)
{
    if (pq->nmemb == 0) {
        return NULL;
    } else {
        return pq->data[0];
    }
}

/* helper function */
inline int parent(int index)
{
    /* parent(i) = (i-1)/2 */
    return ((index-1) >> 1);
}
inline int left(int index)
{
    /* left(i) = i*2 + 1*/
    return ((index << 1) + 1);
}
inline int right(int index)
{
    /* right(i) = i*2 + 2*/
    return ((index << 1) + 2);
}

/* the last item of the Heap do not match the consistency (max-heap)
 *     5         *7*
 *   2    3 =>  2   5
 *  1 0 *7*    1 0 3
 * */
void pqueue_heapify_bottom(pQueue_t *pq)
{ 
    /* note that nmemb points to one after the last item */
    int last = pq->nmemb-1;
    while (last > 0) {
        int paren = parent(last);
        if (pq->cmp(pq->data[last], pq->data[paren]) > 0) {
            /* if cur > parent then exchange them */
            void * tmp = pq->data[last];
            pq->data[last] = pq->data[paren];
            pq->data[paren] = tmp;

            /* and continue the test */
            last = paren;
        } else {
            break;
        }
    }
}

/* partial heap => heap
 * when the top item of the Heap does not match the heap consistency(max-heap)
 */
void pqueue_heapify_top(pQueue_t *pq)
{
    int l,r; /* index of left child/right child */
    int cur = 0;
    int largest = 0;;
    while (cur < pq->nmemb) {
        l = left(cur);
        r = right(cur);

        /* compare the current one the the left child */
        if (l <= pq->nmemb && pq->cmp(pq->data[l], pq->data[cur]) > 0) {
            largest = l;
        } else {
            largest = cur;
        }

        /* compare the previous result to the right child */
        if (r <= pq->nmemb && pq->cmp(pq->data[r], pq->data[largest]) > 0) {
            largest = r;
        }

        /* swap the current one with the largest and continue the loop */
        if (largest != cur) {
            void * tmp = pq->data[cur];
            pq->data[cur] = pq->data[largest];
            pq->data[largest] = tmp;

            cur = largest;
        } else {
            break;
        }
    }
}

/* free the items the pointers points to which were contained in the queue */
void pqueue_free_items(pQueue_t *pq)
{
    int i;
    for (i = 0; i < pq->nmemb; i++) {
        if (pq->data[i] != NULL) {
            free(pq->data[i]);
        }
    }
}

/*****************************************************************************
 * test all the functions above 
 ****************************************************************************/
int numcmp(const void *a, const void *b)
{
    return (*(int*)a - *(int *)b);
}

void print_num_pq(pQueue_t *pq_num)
{
    printf("Items in queue:\n");
    int i;
    for (i = 0; i < pq_num->nmemb; i++) {
        if (pq_num->data[i]) {
            printf("%d ", *(int*)pq_num->data[i]);
        }
    }
    printf("\n");
}

int main(int argc, const char *argv[])
{
    
    printf("test case 1\n");
    int c;

    /* create an empty queue */
    pQueue_t *pq_num = pqueue_init(numcmp, 10);

    int contd = 1;
    while(contd)
    {
        printf("Push(a) | Pop(d) | Get(g) | Print(p) | Quit(q): ");

        do {
            c = getchar();
        } while ( c=='\n' );

        if (c == EOF) {
            break;
        }

        int *new = NULL;
        int *item = NULL;
        switch(c) {
            case 'a':
                new = malloc(sizeof(*new));
                scanf("%d", new);
                pqueue_push(pq_num, new);

                printf("Newly added: %d\n", *new);
                print_num_pq(pq_num);

                new = NULL;
                break;
            case 'd':
                item = pqueue_pop(pq_num);

                if (item == NULL) {
                    printf("Poped: No item\n");
                } else {
                    printf("Poped: %d\n", *item);
                    print_num_pq(pq_num);
                }

                free(item);
                break;
            case 'g':
                item = pqueue_fetch(pq_num);

                if (item == NULL) {
                    printf("Fetched: No item\n");
                } else {
                    printf("Fetched: %d\n", *item);
                    print_num_pq(pq_num);
                }

                break;
            case 'p':
                print_num_pq(pq_num);
                break;

            case 'q':
                contd = 0;
                break;

            default:
                printf("Wrong command!%c\n", c);
                break;
        }

    }

    /* free the queue and items */
    pqueue_free_items(pq_num);
    pqueue_free(pq_num);

    return 0;
}
