DIRS=$(wildcard chap*)

all:
	@for d in $(DIRS); do (cd $$d; $(MAKE)); done



.PHONY: clean
clean:
	@for d in $(DIRS); do (cd $$d; $(MAKE) clean); done
