/* FILE -- kmp.c
 *
 * Implement the KMP string matcher.
 *
 * Sec 32.4
 */
#include <stdio.h>
#include <string.h>
#include <malloc.h>

char *compute_prefix_function(char *pat)
{
    int m = strlen(pat);
    char *next = (char *)malloc(m * sizeof(*next));

    int k = next[0] = -1;
    int q;
    for (q = 1; q < m; ++q) {
        while(k > -1 && pat[k+1] != pat[q]) {
            k = next[k];
        }
        if (pat[k+1] == pat[q]) {
            k++;
        }
        next[q] = k;
    }
    
    return next;
}

int kmp_matcher(char *text, char *pat)
{
    int n = strlen(text);
    int m = strlen(pat);
    char *next = compute_prefix_function(pat);
    int q = -1;

    int i;
    for (i = 0; i < n; ++i) {
        while(q > -1 && pat[q+1] != text[i]) {
            q = next[q];
        }
        if (pat[q+1] == text[i]) {
            q++;
        }

        if (q == m-1) {
            goto found;
        }
    }

found:
    free(next);
    return q>=m-1? (i-m)+1: -1;
}

/* A bit of testing */
int main(int argc, char *argv[])
{
    char *pat = "ababc";
    char *text1 = "aaaaababcddddabc";
    char *text2 = "aaacdb";
    char *text3 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaababc";
    int tmp;

    tmp = kmp_matcher(text1, pat);
    printf("'%s' %s in '%s' at shift %d\n", pat, tmp==-1?"not found":"found",
           text1, tmp);

    tmp = kmp_matcher(text2, pat);
    printf("'%s' %s in '%s' at shift %d\n", pat, tmp==-1?"not found":"found",
           text2, tmp);

    tmp = kmp_matcher(text3, pat);
    printf("'%s' %s in '%s' at shift %d\n", pat, tmp==-1?"not found":"found",
           text3, tmp);

    return 0;
}
