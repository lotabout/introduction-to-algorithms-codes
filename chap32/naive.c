/* FILE -- naive.c
 *
 * Implement naive string-matching algorithm
 *
 * Note that it takes O(n^2) in the worst case.
 *
 * Sec 32.1
 */
#include <stdio.h>
#include <string.h>

/* Return the shift s of text where T[s..m] matches P[s..m] 
 * -1 if not found */
int naive_string_matcher(char *text, char *pat)
{
    int n = strlen(text);
    int m = strlen(pat);
    int i;

    for (i = 0; i <= n-m; ++i) {
        if (strcmp(text+i, pat) == 0) {
            return i;
        }
    }

    return -1;
}

/* A bit of testing */
int main(int argc, char *argv[])
{
    char *pat = "abc";
    char *text1 = "aaaaabcddddabc";
    char *text2 = "aaacdb";
    char *text3 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabc";
    int tmp;

    tmp = naive_string_matcher(text1, pat);
    printf("'%s' %s in '%s' at shift %d\n", pat, tmp==-1?"not found":"found",
           text1, tmp);

    tmp = naive_string_matcher(text2, pat);
    printf("'%s' %s in '%s' at shift %d\n", pat, tmp==-1?"not found":"found",
           text2, tmp);

    tmp = naive_string_matcher(text3, pat);
    printf("'%s' %s in '%s' at shift %d\n", pat, tmp==-1?"not found":"found",
           text3, tmp);

    return 0;
}
