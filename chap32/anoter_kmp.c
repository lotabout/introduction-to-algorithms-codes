/* FILE -- another_kmp.c
 *
 * Implement the KMP string matcher.
 *
 * The difference between this and 'kmp.c' is the meaning of 'next' array.
 *
 * Sec 32.4
 */
#include <stdio.h>
#include <string.h>
#include <malloc.h>

int *compute_prefix_function(const char *pat)
{
    int len_pat = strlen(pat);
    int *next = (int *)malloc(len_pat * sizeof(*next));
    if (next == NULL) {
	return NULL;
    }

    int i = 0;
    int j = next[0] = -1;
    while(i < len_pat-1) {
        while(j > -1 && pat[i] != pat[j]) {
            j = next[j];
        }
        i++;
        j++;
        if (pat[i] == pat[j]) {
            next[i] = next[j];
        } else {
            next[i] = j;
        }
    }
    
    return next;
}

int kmp_matcher(const char *text, const char *pat)
{
    int len_text = strlen(text);
    int len_pat = strlen(pat);
    int *next = compute_prefix_function(pat);
    if (next == NULL) {
	return -2;
    }

    int i = -1;
    int j = -1;
    while(i < len_text) {
        while(j > -1 && text[i] != pat[j]) {
            j = next[j];
        }
        i++;
        j++;
        if (j >= len_pat) {
            break;
        }
    }

    return j >= len_pat ? i-len_pat : -1;
}

/* A bit of testing */
int main(int argc, char *argv[])
{
    char *pat = "ababc";
    char *text1 = "aaaaababcddddabc";
    char *text2 = "aaacdb";
    char *text3 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaababc";
    char *text4 = "a";
    char *pat2= "a";

    int tmp;

    tmp = kmp_matcher(text1, pat);
    printf("'%s' %s in '%s' at shift %d\n", pat, tmp==-1?"not found":"found",
           text1, tmp);

    tmp = kmp_matcher(text2, pat);
    printf("'%s' %s in '%s' at shift %d\n", pat, tmp==-1?"not found":"found",
           text2, tmp);

    tmp = kmp_matcher(text3, pat);
    printf("'%s' %s in '%s' at shift %d\n", pat, tmp==-1?"not found":"found",
           text3, tmp);

    tmp = kmp_matcher(text4, pat2);
    printf("'%s' %s in '%s' at shift %d\n", pat2, tmp==-1?"not found":"found",
           text4, tmp);

    return 0;
}
