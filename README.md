## Purpos of this repo.

Implement some of the algorithms introduced in the book "Introduction to
Algorighms" in C.

## How to compile.
All source codes are independent and is implemented in pure C.

Execute `make` under the top directory to compile all. `make clean` to clean
all generated executable file.

### compile a single executable file.
Note that each C source file could be compiled into executable file. Enter the
subdirectory contained the source code: "qsort.c" for example. Then issue a
command `make qsort` (the file name without extension) will do the good.

## Licence
All the files in this repository are distributed under MIT License. See file
"LICENSE" for detail.

