/* FILE -- radix sort
 *
 * implement radix sort.
 */

#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>


/* radix sort
 *
 * all inputs should be unsigned(i.e. >= 0) */
void radix_sort(unsigned int *base, size_t nmemb)
{
    /* first, calculate floor(lg N)
     * we'll need this to determine the best choice of r 
     *
     * For example, computer is 32-bits, so the maximum number of elements
     * stored in memeory n<=2^32(regard of extended memory), and the bits of a
     * data(in this case, unsigned int) b=32. That means b>floor(lg n).
     * In this case, choose radix bits r=floor(lg n) would get the best
     * performance. */
    int b = sizeof(*base)*CHAR_BIT;
    int r = 0;
    unsigned int mask = 0;  /* mask out r bits of the data */
    int tmp = nmemb;
    while (tmp >>= 1) {
        r++;
        mask = mask << 1 | 0x1;
    }

    /* allocate temp array for counting sort, need 2^r elements*/
    size_t nHash = 1<<r;  /* 2^r */
    size_t hash_size = nHash * sizeof(unsigned int);
    unsigned int *hash = (unsigned int *)malloc(hash_size);
    if (hash == NULL) {
        fprintf(stderr, "Error: radix_sort: Out of memeory.\n");
        exit(-1);
    } 

    /* allocate temp array for storing sorted elements */
    unsigned int *temp_base = (unsigned int *)malloc(nmemb * sizeof(*temp_base));
    if (temp_base == NULL) {
        fprintf(stderr, "Error: radix_sort: Out of memeory.\n");
        exit(-1);
    } 

    /* begin radix sort */
    int i,j;
    unsigned int *src = base;
    unsigned int *dst = temp_base;
    
    /* have to iterate for ceil(b/r) times */
    int count = (b+1)/r;

    for (i = 0; i < count; i++) {
        /* for each radix, perform a counting sort */

        /* initialize hash array(containing number counts) */
        memset(hash, 0, hash_size);

        /* 1. count elements */
        for (j=0; j<nmemb; j++) {
            unsigned int tmp = (src[j] >> (i*r)) & mask;
            hash[tmp] ++;
        }

        /* 2. accumulate the count in hash array */
        for (j = 1; j < nHash; j++) {
            hash[j] += hash[j-1];
        }

        /* 3. resort the original data */
        for (j=nmemb-1; j>=0; j--) {
            unsigned int tmp = (src[j] >> (i*r)) & mask;
            dst[hash[tmp]-1] = src[j];
            hash[tmp]--;
        }

        /* swap src and dest pointers */
        unsigned int *tmp;
        tmp = src;
        src = dst;
        dst = tmp;
    }

    /* copy all the sorted data to base */
    if (dst != base) {
        for (i = 0; i < nmemb; i++) {
            base[i] = dst[i];
        }
    } 

    /* free */
    free(temp_base);
    free(hash);
}

/******************************************************************************
 * Test case
 *****************************************************************************/
void print_array(int *base, int nmemb)
{
    int i;
    for (i = 0; i < nmemb; i++) {
        printf("%3d", base[i]);
        if (i%10 == 9) {
            printf("\n");
        } else {
            printf(" ");
        }
    }
}

int main(int argc, const char *argv[])
{
    srand(time(NULL));

    #define ARRAY_SIZE 100
    unsigned int src[ARRAY_SIZE];

    int i;
    for (i = 0; i < ARRAY_SIZE; i++) {
        src[i] = rand() % 1000;
    }

    printf("\nbefore:\n");
    print_array(src, ARRAY_SIZE);
    radix_sort(src, ARRAY_SIZE);
    printf("\nafter:\n");
    print_array(src, ARRAY_SIZE);


    return 0;
}
