/* FILE -- counting_sort.c
 *
 * Implement counting sort -- a demo.
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

/* note that numbers in src should be positive */
void counting_sort(int *src, int *dst,  int nmemb)
{
    /* find the max item of the input and create an array dynamically, this
     * array is used as a hash table storing the count of element in the input
     * array 
     *
     * This step has nothing to do with counting sort actaully. */
    int i;
    int max = INT_MIN;
    for (i = 0; i < nmemb; i++) {
        if (max < src[i]) {
            max = src[i];
        }
    }

    int *hash = (int*) malloc((max+1) * sizeof(*hash)); /* [0, max] => max+1 */
    if (hash == NULL) {
        fprintf(stderr, "Error: counting_sort: out of memeory\n");
        exit(-1);
    }

    /* initialize the hash array elements to 0 */
    memset(hash, 0, (max+1)*sizeof(*hash));

    /* Begin counting sort */

    /* 1. count the occurences of number */
    for (i = 0; i < nmemb; i++) {
        hash[src[i]] ++;
    }

    /* 2. accumulate the count of numbers in the hash array */
    for (i = 1; i <= max; i++) {
        hash[i] += hash[i-1];
    }

    /* 3. resort the array */
    for (i=nmemb-1; i>=0; --i) {
        dst[hash[src[i]]-1] = src[i];
        hash[src[i]] --;
    }

    free(hash);
}

/******************************************************************************
 * Test case
 *****************************************************************************/
void print_array(int *base, int nmemb)
{
    int i;
    for (i = 0; i < nmemb; i++) {
        printf("%3d", base[i]);
        if (i%10 == 9) {
            printf("\n");
        } else {
            printf(" ");
        }
    }
}

int main(int argc, const char *argv[])
{
    srand(time(NULL));

    #define ARRAY_SIZE 50
    int src[ARRAY_SIZE];
    int dst[ARRAY_SIZE];

    int i;
    for (i = 0; i < ARRAY_SIZE; i++) {
        src[i] = rand() % 100;
    }

    counting_sort(src, dst, ARRAY_SIZE);

    printf("\nsrc:\n");
    print_array(src, ARRAY_SIZE);
    printf("\ndst:\n");
    print_array(dst, ARRAY_SIZE);


    return 0;
}
