/* FILE -- simple qsort 
 *
 * Implement a simple qsort for test.
 *
 * This implementation follows the psudocode introduced in "Introduction to
 * Algorithms"
 */

#include <stdio.h>
#include <stdlib.h>

void sqsort(int *base, int left, int right)
{
    int middle = 0; 
    int i,j,x;

    if (left < right) {
        /* partition */
        x = base[right];    /* right most item */
        i = left - 1;       /* the one before left most item */
        for (j=left; j<=right-1; j++) {
            if (base[j] <= x) {
                ++i;
                /* swap A[i] && A[j] */
                int tmp = base[i];
                base[i] = base[j];
                base[j] = tmp;
            }
        }
        /* swap A[i+1] && A[r] */
        base[right] = base[i+1];
        base[i+1] = x;

        /* recursive call */
        sqsort(base, left, i);
        sqsort(base, i+2, right);
    } else {
        return;
    }
}

void print_array(int *base, int nmemb)
{
    int i;
    for (i = 0; i < nmemb; i++) {
        printf("%d ", base[i]);
    }
}

int main(int argc, const char *argv[])
{
    /* generate a random array and sort them */
    int array[10];
    int i;
    srand(time(NULL));
    
    for (i = 0; i < 10; i++) {
        array[i] = rand()%1000;
    }

    printf("\nbefore:\n");
    print_array(array, 10);
    sqsort(array, 0, 9);
    printf("\nafter:\n");
    print_array(array, 10);
    printf("\n");
    
    
    return 0;
}
