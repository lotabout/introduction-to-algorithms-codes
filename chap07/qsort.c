/* FILE -- qsort.c
 *
 * Implement a general qsort(like it in libc)
 *
 * Borrow some idea and code from libc.
 *
 * This quick sort implementation uses an insertion sort to deal with cases
 * that the size is less than MAX_THRESH. This will enhance its performance.
 * Anyway, change MAX_THRESH to '0' to get a pure quick sort.
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>


/* swap two items */
inline void swap(void *a, void*b, size_t size)
{
    if (a != b) {
        int i;
        char *pl = (char *)a;
        char *pr = (char *)b;
        for (i = 0; i < size; pl++,pr++,i++) {
            *pl ^= *pr;
            *pr ^= *pl;
            *pl ^= *pr;
        }
    }
}

/* do not use partition to sort under this size
 * Because it will be inefficient */
#define MAX_THRESH 4

/* stack node declaration used to store partition pointers */
typedef struct
{
    char *lo;
    char *hi;
} stack_node_t;

/* implement a very fast stack operation */
#define STACK_SIZE (CHAR_BIT * sizeof(size_t))
#define PUSH(low, high) ((void) ((top->lo = (low)), (top->hi = (high)), ++top))
#define POP(low, high)  ((void) (--top, (low = top->lo), (high = top->hi)))
#define STACK_NOT_EMPTY (stack < top)

void mqsort(void *base, size_t nmemb, size_t size, 
            int (*cmp)(const void *, const void *))
{
    char *base_ptr = (char *)base;

    const size_t max_thresh = MAX_THRESH * size;

    if (nmemb == 0) {
        /* avoid the lossage with unsigned arithmetic used below */
        return ;
    }

    if (nmemb > MAX_THRESH) {
        char *lo = base_ptr;
        char *hi = lo + (nmemb-1)*size;

        /* decalre a stack and push the first element into the stack to start
         * loop */
        stack_node_t stack[STACK_SIZE];
        stack_node_t *top = stack;
        PUSH(NULL, NULL);

        while (STACK_NOT_EMPTY) {
            char * left_ptr = NULL;
            char * right_ptr = NULL;
            
            /* doing the partition */

            /* choose a medium pivot between the left/middle/right 
             * after this, the array looks like [min ... mid ... max]*/
            char * mid = lo + size*((hi - lo)/size >> 1);

            if (cmp((void *)mid, (void *)lo) < 0) {
                swap(mid, lo, size);
            }
            /* after the previous step, we garentee that left < mid */

            if (cmp((void *)hi, (void *)mid) < 0) {
                swap(hi, mid, size);

                /* we need another check cause now we get: 
                 * (left < mid) && * (mid > right), after exchange, we don't
                 * know whether (left < right) or not? */
                if (cmp((void *)mid, (void *)lo) < 0) {
                    swap(mid, lo, size);
                }
            }
            /* now we have [left < mid < right] */

            left_ptr = lo + size;
            right_ptr = hi - size;

            /* begin partition */
            do {
                while (cmp((void *)left_ptr, (void *)mid) < 0) {
                    left_ptr += size;
                }

                while (cmp((void *)mid, (void *)right_ptr) < 0) {
                    right_ptr -= size;
                }

                /* update mid pointer */
                if (left_ptr < right_ptr) {
                    swap(left_ptr, right_ptr, size);

                    if (mid == left_ptr) {
                        mid = right_ptr;
                    } else if (mid == right_ptr) {
                        mid = left_ptr;
                    }
                    left_ptr += size;
                    right_ptr -= size;
                } else if (left_ptr == right_ptr) {
                    left_ptr += size;
                    right_ptr -= size;
                    break;
                }
            } while (left_ptr <= right_ptr);

            /* now pointers are like 
             * [lo ... right_ptr . mid . left_ptr ... high]
             */

            /* setup pointers for next iteration */
            if ((size_t)(right_ptr - lo) <= max_thresh) {
                if ((size_t)(hi - left_ptr) <= max_thresh) {
                    /* ignore both small parts */
                    POP(lo, hi);
                } else {
                    /* ignore the left side */
                    lo = left_ptr;
                }
            } else if ((size_t)(hi - left_ptr) <= max_thresh) {
                /* ignore the right side */
                hi = right_ptr;
            } else if ((hi - left_ptr) > (right_ptr - lo)) {
                /* store the large right part */
                PUSH(left_ptr, hi);
                hi = right_ptr;
            } else {
                /* store the large left part */
                PUSH(lo, right_ptr);
                lo = left_ptr;
            }

            /* store the pointers of the large part */
        }
    }

    /* after the array have been *partial sorted* by quick sort, we use
     * insertion sort to sort the remaining fragments */
#define min(x,y) ((x)<(y)?(x):(y))
    {
        char * const end_ptr = base_ptr + size*(nmemb-1);
        char * tmp_ptr = base_ptr;
        char * thresh = min(end_ptr, base_ptr + max_thresh);
        char * run_ptr = NULL;

        /* first, put the smallest item to the first as a pivot 
         * this will reduce the comparison we have to take while doing
         * insertion sort */
        for (run_ptr = tmp_ptr+size; run_ptr <= thresh; run_ptr+=size) {
            if (cmp((void *)run_ptr, (void *)tmp_ptr) < 0) {
                tmp_ptr = run_ptr;
            }
        }

        if (tmp_ptr != base_ptr) {
            swap((void *)base_ptr, (void *)tmp_ptr, size);
        }

        /* doing insersion sort 
         * run_ptr starts from the third item:
         * [min, 1, run_ptr ... ]*/
        run_ptr = base_ptr + size;
        while ((run_ptr += size) <= end_ptr) {
            /* tmp_ptr starts from the one before run_ptr and look backwards
             * to find the correct insert position */
            tmp_ptr = run_ptr - size;
            while (cmp((void *)run_ptr, (void *)tmp_ptr) < 0) {
                tmp_ptr -= size;
            }

            tmp_ptr += size; /* this is the correct insertion place */
            if (tmp_ptr != run_ptr) {
                /* move the items between tmp_ptr and run_ptr forward and
                 * insert run_ptr to the newly place.
                 * [backward <----> forward] */

                char * trav; /* running pointer in bits in 
                                [run_ptr, run_ptr+size-1] */

                trav = run_ptr + size;
                while (--trav >= run_ptr) {
                    char c = *trav; /* tmp variable */
                    char *hi;
                    char *lo;
                    for (lo=hi=trav; (lo-=size)>=tmp_ptr; hi=lo) {
                        *hi = *lo;
                    }
                    *hi = c;
                }
            }
        }
    } /* end of insersion sort */

}

/* test the previous sort function */
int numcmp(const void *a, const void *b)
{
    return (*(int*)a - *(int*)b);
}

void print_array(int *base, int nmemb)
{
    int i;
    for (i = 0; i < nmemb; i++) {
        printf("%3d", base[i]);
        if (i%10 == 9) {
            printf("\n");
        } else {
            printf(" ");
        }
    }
}


int main(int argc, const char *argv[])
{
    #define ARRAY_SIZE 100
    int array[ARRAY_SIZE];
    srand(time(NULL));
    int i;
    for (i = 0; i < ARRAY_SIZE; i++) {
        array[i] = rand()%1000;
    }

    printf("before:\n");
    print_array(array, ARRAY_SIZE);
    mqsort(array, ARRAY_SIZE, sizeof(*array), numcmp);
    printf("\nafter:\n");
    print_array(array, ARRAY_SIZE);
    printf("\n");
    
    return 0;
}
